<?php

header('Content-Type: application/json');

require_once('../../common/lib.php');
require_once('../../common/define.php');

/**
 * Como crear un cargo a una tarjeta usando Culqi PHP.
 */

// Usando Composer (o puedes incluir las dependencias manualmente)
//require '../vendor/autoload.php';
// require_once get_home_path().'../vendor/autoload.php';
require_once('../../vendor/autoload.php');

// Cargar Culqi-PHP de forma manual
//require 'vendor/culqi/culqi-php/lib/culqi.php';

//use Culqi\Culqi;

$culqi_key_secret = "";
                        $result_culqi = $db->query('SELECT b.id_hotel, b.key_public, b.key_secret
                                                    FROM pm_booking a
                                                    inner join pm_culqi_payment b on b.id_hotel = a.id_hotel
                                                    where a.id = '.$_POST["orderid"].' ');
                        if($result_culqi !== false){
                            foreach($result_culqi as $row){                                
                                $culqi_key_secret = $row['key_secret'];
                            }
                        }

// Configurar tu API Key y autenticación
$SECRET_API_KEY = $culqi_key_secret;
$culqi = new Culqi\Culqi(array('api_key' => $SECRET_API_KEY));

try {

  // Creando Cargo a una tarjeta
  $charge = $culqi->Charges->create(
      array(
          "amount" => $_POST["amount"],
          "capture" => true,
          "currency_code" => "PEN",
          "description" => $_POST["description"],
          "email" => $_POST["email"],
          "installments" => (int)$_POST["installments"],
          "source_id" => $_POST["token"],
          // "antifraud_details" => [
          //   "address"=> "",
          //   "address_city"=> "",
          //   "country_code"=> "",
          //   "first_name"=> $_POST["firstname"],
          //   "last_name"=> $_POST["lastname"],
          //   "phone_number"=> 0
          // ],
          "metadata" => ["ORDER_ID" => $_POST["orderid"]]
      )
  );
  // Response
  echo json_encode($charge);

} catch (Exception $e) {

  echo json_encode($e->getMessage());

}
