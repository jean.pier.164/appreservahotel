<?php
define('SITE_TITLE', 'Hoteles Peru');
define('TIME_ZONE', 'America/Lima');
define('DATE_FORMAT', '%e %B %Y');
define('TIME_FORMAT', '%I:%M%P');
define('CURRENCY_ENABLED', '0');
define('CURRENCY_POS', 'after'); // before or after
define('LANG_ENABLED', '0');
define('ADMIN_LANG_FILE', 'es.ini');
define('ENABLE_COOKIES_NOTICE', '1');
define('MAINTENANCE_MODE', '0');
define('MAINTENANCE_MSG', '<h1><i class=\"fa fa-rocket\"></i> Próximamente ...</h1><p> Lo sentimos, nuestro sitio web no funciona por mantenimiento. </p>');
define('TEMPLATE', 'default');
define('OWNER', 'Jose Bazan');
define('EMAIL', 'jbazand@hotelesperu.net');
define('ADDRESS', 'Lima - Peru');
define('PHONE', '922284186');
define('MOBILE', '');
define('FAX', '');
define('DB_NAME', 'hoteles_reservas');
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_USER', 'hoteles_reservas');
define('DB_PASS', 'Elricotelo2020#');
define('SENDER_EMAIL', '');
define('SENDER_NAME', '');
define('USE_SMTP', '0');
define('SMTP_SECURITY', '');
define('SMTP_AUTH', '0');
define('SMTP_HOST', '');
define('SMTP_USER', '');
define('SMTP_PASS', '');
define('SMTP_PORT', '25');
define('GMAPS_API_KEY', '');
define('ANALYTICS_CODE', '');
define('ADMIN_FOLDER', 'admin');
define('CAPTCHA_PKEY', ''); // ReCAPTCHA public key
define('CAPTCHA_SKEY', ''); // ReCAPTCHA secret key
define('AUTOGEOLOCATE', '0'); // Change the currency according to the country (https required)
define('PAYMENT_TYPE', 'arrival,culqi'); // 2checkout,paypal,check,arrival
define('PAYPAL_EMAIL', '');
define('VENDOR_ID', ''); // 2Checkout.com account number
define('SECRET_WORD', ''); // 2Checkout.com secret word
define('PAYMENT_TEST_MODE', '1');
define('ENABLE_DOWN_PAYMENT', '1');
define('DOWN_PAYMENT_RATE', '30'); // %
define('DOWN_PAYMENT_AMOUNT', '50'); // amount required to activate the down payment
define('ENABLE_TOURIST_TAX', '1');
define('TOURIST_TAX', '0');
define('TOURIST_TAX_TYPE', 'fixed');
define('ALLOW_COMMENTS', '1');
define('ALLOW_RATINGS', '1'); // If comments enabled
define('ENABLE_BOOKING_REQUESTS', '0'); // Possibility to make a reservation request if no availability
define('ENABLE_MULTI_VENDORS', '1'); // Payments are made on the PayPal account of the hotels
define('BRAINTREE_MERCHANT_ID', '');
define('BRAINTREE_PUBLIC_KEY', '');
define('BRAINTREE_PRIVATE_KEY', '');
define('CURRENCY_CONVERTER_KEY', ''); // currencyconverterapi.com API key
define('SHOW_CALENDAR', '0');
define('RAZORPAY_KEY_ID', '123');
define('RAZORPAY_KEY_SECRET', '123');
define('ENABLE_ICAL', '1');
define('ENABLE_AUTO_ICAL_SYNC', '1');
define('ICAL_SYNC_INTERVAL', 'daily'); // daily | hourly
define('ICAL_SYNC_CLOCK', '3'); // 0-23h mode, required if ICAL_SYNC_INTERVAL = daily
//add fix culqi
define('CULQI_KEY_PUBLIC', 'pk_test_SFz9xw3BSn2eI6GE');
define('CULQI_KEY_SECRET', 'sk_test_SeEqfFulJwekxL38');